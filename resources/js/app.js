require('./bootstrap');
window.Vue = require('vue');
var VueResource = require('vue-resource');
import router from './routes';
Vue.use(VueResource);

const files = require.context('./components', true, /\.vue$/i);
files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

const app = new Vue({
    el: '#app',
    router
});
