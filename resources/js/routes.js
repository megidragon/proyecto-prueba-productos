import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

export default new Router({
    routes: [
        {
            path: '/',
            name: 'products',
            component: require('./views/Products').default
        },



        {
            path: '*',
            component: require('./views/404').default
        },
    ],
    mode: 'history'
});