<?php

use Illuminate\Http\Request;

Route::get('productos', 'ProductsController@index');
Route::get('productos/{id}', 'ProductsController@show');
Route::post('productos', 'ProductsController@store');
Route::put('productos/{id}', 'ProductsController@update');
Route::delete('productos/{id}', 'ProductsController@delete');