<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    protected $appends = ['precio_dolares'];
    protected $fillable = [
        'nombre',
        'precio',
    ];

    public function getPrecioDolaresAttribute()
    {
        return $this->calculateCurrency();
    }

    private function calculateCurrency(){
        return round($this->precio / env('DOLAR_VARIATION', 1), 2);
    }
}
