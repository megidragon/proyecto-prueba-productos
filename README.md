# Proyecto para test.
@Author: Agustin Nicolas Gatta Hofman

Proyecto desarrollado en el framework Laravel usando PHP acompañado de Vue.js para el FrontEnd


### Prerrequisitos

* PHP - Lenguaje principal
* MySQL - Motor de base de datos
* [Composer](https://getcomposer.org/) Gestor de dependencias para PHP
* [NPM](https://www.npmjs.com/) Gestor de dependencias para JavaScript

También se requiere tener las extensiones de php habilitadas:
* MBstring
* BCMath
* JSON
* Ctype
* OpenSSL
* PDO
* Tokenizer
* XML

### Instalación

* Clonar o descomprimir el proyecto en un directorio
* Renombrar el archivo .env.example a .env
* Configurar las credenciales de la base de datos en el archivo .env
* Correr el comando ``` composer install ``` para instalar las dependencias de php dentro del directorio del proyecto
* Correr el comando ``` npm install ``` para instalar las dependencias de javascript dentro del directorio del proyecto
* Correr el comando ``` php artisan key:generate ```
```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=laravel
DB_USERNAME=root
DB_PASSWORD=
```
* Una vez configurado correr el comando ``` php artisan migrate ``` dentro del directorio del proyecto
* Luego correr el comando ``` php artisan db:seed --class=ProductsTableSeeder ```
* Una vez finalizado los pasos anteriores puede levantar el proyecto localmente usando el comando ``` php artisan serve ```
