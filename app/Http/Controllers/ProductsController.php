<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Products;

class ProductsController extends Controller
{
    /**
     * @return Products[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
        return Products::query()->orderBy('id', 'desc')->get();
    }

    /**
     * @param Products $article
     * @return Products
     */
    public function show($id)
    {
        return response()->json(Products::query()->find($id), 200);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $product = Products::create($request->all());

        return response()->json($product, 201);
    }

    /**
     * @param Request $request
     * @param Products $article
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $product = Products::query()->find($id);
        $product->update($request->all());

        return response()->json($product, 200);
    }

    /**
     * @param Products $article
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function delete($id)
    {
        Products::query()->find($id)->delete();

        return response()->json(['success' => true], 204);
    }
}
